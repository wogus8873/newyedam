package com.yedam.inter2;

public class CarEx {
	public static void main(String[] args) {
		
		Car myCar = new Car();
		myCar.run();
		
		myCar.frontLeftTire = new KumhoTire();
		myCar.backRightTire = new KumhoTire();
		
		myCar.run();
		
		
		
	}
}
