package com.yedam.inter2;

public interface InterFaceC extends InterFaceA,InterFaceB{
	//InterFaceA와InterFaceB의 내용이 담긴 인터페이스
	//A기능+B기능+C기능 다 가짐
	public void methodC();
}
