package com.yedam.inter;

public class WashingEx {
	public static void main(String[] args) {
		WashingMachine LGws = new LgWashingMachine();
		LGws.startBtn();
		LGws.pauseBtn();
		System.out.println("세탁기 속도 " + LGws.changeSpeed(2) +" 로 변경했습니다");
		LGws.stopBtn();
		
		LGws.dry();
	}
}
