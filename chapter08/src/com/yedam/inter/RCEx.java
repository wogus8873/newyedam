package com.yedam.inter;

public class RCEx {
	public static void main(String[] args) {
		
		
		RemoteControl rc = new SmartTv();  //자동타입변환 자식클래스의 재정의된 메소드 출력
											  //인터페이스는 자기스스로 객체화 할 수 없기때문에 자동변환으로 자식을 객체화해서 부모를 같이 객체화함
											  //그래서 턴온 턴오프 됨
		
		//smarttv - > implements remoteconcr(+searchable)
		//리모트컨트롤+서치에이블 -> 서치에으블을 상속받고있어서
		//리모트컨트롤
		rc.setVolume(40);//텔레비전이 가진 메소드
		rc.turnOn();//리모컨이 가진 메소드
		rc.turnOff();//리모컨이 가진 메소드
		//서치에이블
		rc.search("wew.asdfa.wefae");
		
		rc = new Audio();
		
		rc.setVolume(5);   //오디오가 가진 메소드
		
		rc.turnOn(); //리모컨이 가진 메소드
		rc.turnOff();//리모컨이 가진 메소드
		
		
			
		Television tv = new Television();  //tv만 쓸거면 이렇게해도 상관없음.
										   // 다형성을 위해 위에처럼 한거임
		tv.turnOn();
		tv.setVolume(5);
		tv.turnOff();
		
		
	
		
		
		
		
	}
}
