package com.yedam.abs;

public class AnimalEx {
	public static void main(String[] args) {
		Cat cat = new Cat();
		
		cat.sound();
		System.out.println();
		
		Animal animal = new Cat();
			  //추상클래스는 자기자신을 객체로 만들지 못하는데 자식클래스를 객체로 해서 객체로?..머곴ㅃ
		animal.sound();
		animal.breathe();
		System.out.println(animal.kind);
		
		
		animalSound(new Cat());
		//추상클래스 -> 클래스와 별반 다르지 않다.
		//차이점 
		//1)자기자신을 객체로 만들지 못함
		//2)추상메소드가 존재하면 상속을 받게되면 추상 메소드는 반드시 구현해야한다
		//3)스스로 객체(인스턴스)화가 안 되므로 자식클래스를 통한 자동타입변환으로 구현.
		
	}
	
	//매개 변수를 활용한 자동타입변환
	public static void animalSound(Animal animal) {
		animal.sound();  //cat에 재정의된 
	}
	
	
	
	
	
	
}
