package com.yedam.access;

import com.yedam.inheri.Parent;

public class Child extends Parent {
	//다른패키지에서 상속하려고하면 import써서 사용해야함
	public String lastName;
	public int age;
	
	
	public Child() {
		
	}
	//부모에 protexted 걸어서 상속간이라 다른 패키지도 연결됨
	public void showInfo() {
		System.out.println("내 성 : "+firstName);
		System.out.println("DNA : "+DNA);
	}
	
	
}

