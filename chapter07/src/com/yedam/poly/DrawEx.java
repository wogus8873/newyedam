package com.yedam.poly;

public class DrawEx {
	public static void main(String[] args) {
		//자동타입변환
		//부모타입변수 = new 자식클래스()
		//Draw 에 있는것만 쓸 수 있음
		//Circle(자식) 에서 재정의(오버라이딩) 된게 있으면 그것을 실행하는것
		//다형성을 구현
		
		Draw figure = new Circle();
		
		figure.x = 1;
		figure.y = 2;
		figure.draw();     
		
		figure = new Rectangle();
		
		figure.draw();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
