package com.yedam.array;

public class Exam08 {
	public static void main(String[] args) {
		
		
		//객체 참조 배열 ㅔ193
		String[] strArray = new String[3];
		strArray[0] = "yedam";
//		strArray[0] = str = "yedam";
		strArray[1] = "yedam";
		strArray[2] = new String("yedam");  //문자열은 같으나 주소는 다름
		
		System.out.println(strArray[0] == strArray[1]);
		System.out.println(strArray[0] == strArray[2]);
		System.out.println(strArray[0].equals(strArray[2]));
		
		
		
		//배열복사
		int[] oldArray = {1,2,3};
		int[] newArray = new int[5];
		
		for(int i=0; i<oldArray.length; i++) {
			newArray[i] = oldArray[i];  //for 문을 사용해서 newArray에 배열복사함.
		}
		for(int i=0; i<newArray.length; i++){
			System.out.println(newArray[i]); //데이터 3개 넣어서 나머지 2개는 0 0 로 표시 / oldArray 복사 잘 됐는지 보려고 만든것.
		}
		
		
		
		int[] oldArray2 = {1,2,3,4,5,6,7};
		int[] newArray2 = new int[10];
		
		System.arraycopy(oldArray2,0,newArray2,0,oldArray2.length); //복사
		
		for(int i=0; i<newArray2.length; i++) {
			System.out.println(newArray2[i]);
		}
		
		
		//향상된 for 문
		
		for(int temp : newArray2) {
			System.out.print(temp+"\t");
		}
		
		
		
		
		
		
		
		
		
		
		
	}
}
