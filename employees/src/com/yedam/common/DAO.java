package com.yedam.common;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import sun.print.PeekGraphics;

public class DAO {
	//DAO -> DATA ACCESS OBJECT
	//JAVA -> DB 연결해주는 객체(JDBC)
	//OJDBC를 가져와서 자바프로젝트에 추가 
	
	//java -> DB 연결할 때 쓰는 객체
	protected Connection conn = null;
	//java.sql임포트 
	
	//Select(조회) 결과 값 반환 받는 객체 
	protected ResultSet rs = null;
	
	//Query문 을 가지고 실행하는 객체
	protected PreparedStatement pstmt = null;
	//Query문 을 가지고 실행하는 객체
	protected Statement stmt = null;
	
	//SELECT , INSERT , UPDATE , DELETE 등등의 Query 문을
	//DB로 가져가서 실행시킴.
	//Ex) SELECT * FROM employees;
	//접속시도(접속정보)
	Properties pro = new Properties();
	String driver = "";
								   //IP      :PORT:DBname
	String url = "";
	String id = "";
	String pw = "";
	
	//DB연결 메소드
	public void conn() {
		
		try {
		getProperties();
		//1. 드라이버 로딩
		Class.forName(driver);
		//2. DB 연결
		
		conn = DriverManager.getConnection(url,id,pw);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void disconn() {
		try {
			if(rs != null) {
				rs.close();
			}
			if(stmt != null) {
				stmt.close();
			}
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//DB접속 정보 호출 메소드
	private void getProperties() {
		try {
			FileReader resource = new FileReader("src/config/db.properties");
			pro.load(resource);
			driver = pro.getProperty("driver");
			url = pro.getProperty("url");
			id = pro.getProperty("id");
			pw = pro.getProperty("pw");
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	//DB교체가 용이하다 
	//JAVA 랑 실행에 상관없는 메모장 내용을 읽어와서 DB를 실행
	//내가 원할때 마ㅏ다 메모장 내용 수정해주면 각기다른 DB에 접속할 수 있다.
	
	//개발자가 아닌 유지보수 인원,프로그램 관리자들도 DB를 교체 할 수 있다.
	
	//치명적인 데이터(환경설정 내용) 아니면 파일로 저장해서 유지보수,프로그램관리자도 사용할 수 있게 해준다.
}
