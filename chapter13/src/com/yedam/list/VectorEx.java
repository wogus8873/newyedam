package com.yedam.list;


import java.util.List;
import java.util.Vector;



public class VectorEx {
	public static void main(String[] args) {
		List<Board> list = new Vector<>();
		
		list.add(new Board("제목1","내용1","글쓴이1"));
		//list.add(0) 제목내용글쓴이1이 담긴 객체가 담김
		list.add(new Board("제목2","내용2","글쓴이2"));
		list.add(new Board("제목3","내용3","글쓴이3"));
		list.add(new Board("제목4","내용4","글쓴이4"));
		list.add(new Board("제목5","내용5","글쓴이5"));
		
		for(int i=0; i<list.size(); i++) {
			//첫번째방법
//			Board board = list.get(i);
//			System.out.println(board.subject+"\t"+board.content +"\t"+board.writer);
			
			//두번째 방법 책애업슨ㄴ거  표현방법만 다르지 똑같음
			//변수에 담지않고 list.get 바로 사용
			System.out.println(list.get(i).subject+"\t"+list.get(i).content+"\t"+list.get(i).writer);
		}
		
		System.out.println();
		
		list.remove(2);

		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i).subject+"\t"+list.get(i).content+"\t"+list.get(i).writer);
		}
		
		
		
		
		
	}
}
