package com.yedam.exe;

import java.util.Scanner;

import com.yedam.member.MemberService;

public class ExeApp {
	Scanner sc = new Scanner(System.in);
	
	MemberService ms = new MemberService();
	String menu = "";
	public ExeApp() {
		start();
	}
	
	private void start() {
		boolean run = true;
		while(run) {
			//1.로그인이 돼있을때 메뉴
			if(MemberService.memberInfo != null) {
				System.out.println("1.회원조회 | 2.회원등록 | 3.회원탈퇴 | 4.회원수정 | 5.로그아웃");
				menu = sc.nextLine();
				if(menu.equals("1")) {
					ms.getMemberList();
					
				}else if(menu.equals("2")) {
					ms.insertMember();
					
				}else if(menu.equals("3")) {
					ms.deleteMember();
				}else if(menu.equals("4")) {
					ms.updateMember();
				}
				
				else if(menu.equals("5")){
					ms.logout();
					
				}
			}
			
			
			
			//2.로그인이 안 됐을때 메뉴
			else if(MemberService.memberInfo == null) {
				System.out.println("1.로그인 | 2.종료");
				System.out.println("입력 > ");
				menu = sc.nextLine();
				if(menu.equals("1")) {
					ms.login();
				}else if(menu.equals("2")) {
					System.out.println("끝ㅋㅋ");
					run = false;
				}
				
			}
			
			
			
			
			
			
			
			
			
			
			
		}
	}
	
	
}
