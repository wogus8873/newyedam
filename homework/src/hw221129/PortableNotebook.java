package hw221129;

public class PortableNotebook implements Tablet {
	//필드
	public int nowMode;
	public String document;
	public String internet;
	public String video;
	public String app;
	//생성자
	public PortableNotebook() {
		this.nowMode = NOTEBOOK_MODE;
		System.out.println("현재모드 : NOTEBOOK_MODE");
		this.document = "한글2020";
		this.internet = "크롬";
		this.video = "영화";
		this.app = "안드로이드 앱";
		
	}
	
	//메소드

	@Override
	public void writeDocumentaion() {
		// TODO Auto-generated method stub
		System.out.println(document+"을 통해 문서 작성");
	}

	@Override
	public void searchInternet() {
		// TODO Auto-generated method stub
		System.out.println(internet+"을 통해 인터넷 검색");
	}

	@Override
	public void watchVideo() {
		// TODO Auto-generated method stub
		System.out.println(video+"를 시청");
	}

	@Override
	public void useApp() {
		// TODO Auto-generated method stub
		if(nowMode == 1) {
			System.out.println(nowMode+"를 바꾸고 "+app+"을 실행");
			nowMode = TABLET_MODE;
		}else if(nowMode == 2) {
			System.out.println(app+"을 실행");
		}
	}
	
	public void changeMode() {
		if(nowMode == 1) {
			System.out.println("현재모드 : NOTEBOOK_MODE, TABLET_MODE로 변경");
			nowMode = TABLET_MODE;
		}else if(nowMode == 2) {
			System.out.println("현재모드 : TABLET_MODE, NOTEBOOK_MODE로 변경");
			nowMode = NOTEBOOK_MODE;
		}
	}
	
}
