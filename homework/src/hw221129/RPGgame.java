package hw221129;

public class RPGgame implements Keypad{
	//필드
	public int nowMode;
	
	
	//생성자
	public RPGgame() {
		this.nowMode = NORMAL_MODE;
		System.out.println("RPGgame을 실행");
	}
	
	
	//메소드
	@Override
	public void leftUpButton() {
		System.out.println("위쪽으로 이동한다");
	}

	@Override
	public void rightUpButton() {
		if(nowMode == NORMAL_MODE) {
			System.out.println("-NORMAL_MODE : 캐릭터가 한칸단위로 점프한다.");
		}else if(nowMode == HARD_MODE) {
			System.out.println("-HARD_MODE : 캐릭터가 두칸단위로 점프한다.");
		}
	}

	@Override
	public void leftDownButton() {
		System.out.println("아래쪽으로 이동한다");
	}

	@Override
	public void rightDownButton() {
		if(nowMode == 0) {
			System.out.println("- NORMAL_MODE : 캐릭터가 일반 공격.");
		}else if(nowMode == 1) {
			System.out.println("- HARD_MODE : 캐릭터가 HIT 공격.");
		}
	}

	@Override
	public void changeMode() {
		if(nowMode == 0) {
			System.out.println("NORMAL_MODE -> HARD_MODE");
			this.nowMode = HARD_MODE; 
		}else if(nowMode == 1) {
			System.out.println("HARD_MODE -> NORMAL_MODE");
			this.nowMode = NORMAL_MODE;
		}
	}

}
