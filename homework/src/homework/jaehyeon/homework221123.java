package homework.jaehyeon;

import java.util.Scanner;

public class homework221123 {
	public static void main(String[] args) {
		//주어진 배열을 이용하여 다음 내용을 구현하세요.
		int[] arr1 = { 10, 20, 30, 70, 3, 60, -3 };
		//문제1. 주어진 배열 중에서 값이 60인 곳의 인덱스를 출력해보자
		
		for(int i=0; i<arr1.length; i++) {
			if(arr1[i] == 60) {
				System.out.println("60인 곳의 index는"+i);
			}
		}
		
		//문제2. 주어진 배열의 인덱스가 3인 곳은 출력하지 말고, 나머지만 출력해보자
		
		for(int i=0; i<arr1.length; i++) {
			if(arr1[3] != arr1[i]) {
				System.out.println("index3 이외의 값"+arr1[i]);
			}
		}
		
		//문제3. 주어진 배열 안의 변경하고 싶은 값의 인덱스 번호를 입력받아, 그 값을 1000으로 변경해보자.
		Scanner sc = new Scanner(System.in);
		
		System.out.println("인덱스값 입력하세요>");
		int chIdx = Integer.parseInt(sc.nextLine());
		System.out.println("변경할값 입력하세요>");
		int chVal = Integer.parseInt(sc.nextLine());
		
		for(int i=0; i<arr1.length; i++) {
			if(chIdx == i) {
				arr1[i] = chVal;
			}
			System.out.println(arr1[i]);
		}
		
		//문제4. 주어진 배열의 요소에서 최대값과 최소값을 구해보자.
		
		int max = arr1[0];
		for(int i=0; i<arr1.length; i++) {
			if(max<arr1[i]) {
				max = arr1[i];
			}
		}
		System.out.println("최대값 : "+max);
		
		int min = arr1[0];
		for(int i=0; i<arr1.length; i++) {
			if(min>arr1[i]) {
				min = arr1[i];
			}
		}
		System.out.println("최소값 : "+min);
		
		//문제5. 별도의 배열을 선언하여 양의 정수 10개를 입력받아 배열에 저장하고, 배열에 있는 정수 중에서 3의 배수만 출력해보자.
		int any =0;
		
		System.out.println("인덱스갯수 입력하세요>");
		any = Integer.parseInt(sc.nextLine());
		int[] val = null;
		val = new int[any];
		String result = ""; 
		
		for(int i=0; i<val.length; i++) {
			System.out.println("값을 입력하세요>");
			val[i] = Integer.parseInt(sc.nextLine());
				if(val[i]%3 == 0) {
					result += val[i];
				}
		}
		System.out.println(result);
		
	
		
		
		
		
		
		
		
		
		
	}
}
