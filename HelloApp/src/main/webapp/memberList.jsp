<%@page import="co.dev.member.Member"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원목록 페이지</title>
</head>
<body>
	<h3>회원목록</h3>
	<%
		String name = (String) request.getAttribute("userName");
		List<Member> list = (List<Member>) request.getAttribute("memberList");
	%>
	<h3><%=name %></h3>
	<ul>
	<%
		for(Member vo : list){//for 반복문 (자바영역)
	%>
		<li><%=vo.getMemberId()%>,<%=vo.getMemberPw() %></li> <!-- html영역 -->
	<%
		}//for 반복문 (자바영역)
	%>
	</ul>
</body>
</html>