package com.yedam.mybook;

import java.util.List;
import java.util.Scanner;

import com.yedam.book.Book;
import com.yedam.mem.MemService;

import BookReview.BookReview;
import BookReview.BookReviewDAO;

public class MyBookService {
	Scanner sc = new Scanner(System.in);
	
	public void getMyBookList() {
		List<MyBook> list = MyBookDAO.getInstance().getMyBookList();
		
		for(MyBook myBook : list) {
			System.out.println("도서 아이디 : "+myBook.getbId());
			System.out.println("도서 이름 : "+myBook.getbName());
			System.out.println("도서 장르 : "+myBook.getbGenre());
			System.out.println("도서 줄거리 : "+myBook.getbSummary());
			System.out.println("도서 출판사 : "+myBook.getbPublisher());
			System.out.println("구입한 날짜 : "+myBook.getMbDate());
			System.out.println("소장여부 : "+myBook.getMbStatus());
			System.out.println("======================================");
		}
	}
	//결제
	public void payMent() {
		
		int pay = 0;
		String id = MemService.memInfo.getMemId();
		System.out.println("결제 금액을 입력하세요 : ");
		pay = Integer.parseInt(sc.nextLine());
		
		//cash 증가
		MemService.memInfo.setMemCash(MemService.memInfo.getMemCash()+pay);
		MemService.memInfo.setMemTotalCash(MemService.memInfo.getMemTotalCash()+pay);
		int result = MyBookDAO.getInstance().cashPayMent(pay,id);
		
		if(result >= 1) {
			System.out.println("결제 완료");
		}else if(result == 0){
			System.out.println("결제 실패");
		}
		
		//card 감소
		
		int result2 = MyBookDAO.getInstance().cardPayMent(pay, id);
		
		if(result2 == 1) {
			System.out.println("카드에서 "+pay+"원 출금");
		}else if (result == 0) {
			System.out.println("출금 실패");
		}
	}
	
	//대여도서삭제
	public void deleteBook() {
		int result = MyBookDAO.getInstance().delBook();
		
		if(result >= 1 ) {
			System.out.println("====================");
			System.out.println("기간이 만료되어 도서 삭제");
			System.out.println("====================");
		}
		
	}
	
	//추천장르
	public void genreRecommend() {
		
		MyBook mb = MyBookDAO.getInstance().genreRecommend();
		
		if(mb != null) {
			System.out.println("추천장르는 '"+mb.getbGenre()+"' 입니다");
		}else if(mb == null) {
			System.out.println("추천할만한 장르가 없네요");
		}
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
}
