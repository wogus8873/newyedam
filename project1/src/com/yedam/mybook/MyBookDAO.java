package com.yedam.mybook;

import java.util.ArrayList;
import java.util.List;

import com.yedam.book.Book;
import com.yedam.common.DAO;
import com.yedam.mem.Mem;
import com.yedam.mem.MemService;

public class MyBookDAO extends DAO{
	private static MyBookDAO myBookDao = null;
	private MyBookDAO() {
		
	}
	public static MyBookDAO getInstance() {
		if(myBookDao == null) {
			myBookDao = new MyBookDAO();
		}
		return myBookDao;
	}
///////////////////////////////////////////////////////////////////////
//	B_ID           NUMBER(10)    
//	B_NAME         VARCHAR2(30)  
//	B_GENRE        VARCHAR2(30)  
//	B_SUMMARY      VARCHAR2(500) 
//	B_PUBLISHER    VARCHAR2(30)  
//	MB_DATE        DATE          
//	MB_STATUS      VARCHAR2(3)   

	
	
	//내 도서전체조회
	public List<MyBook> getMyBookList(){
		List<MyBook> list = new ArrayList<>();
		MyBook myBook = null;
		
		try {
			conn();
			String sql = "select * from mybook";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				myBook = new MyBook();
				myBook.setbId(rs.getInt("b_id"));
				myBook.setbName(rs.getString("b_name"));
				myBook.setbGenre(rs.getString("b_genre"));
				myBook.setbSummary(rs.getString("b_summary"));
				myBook.setbPublisher(rs.getString("b_publisher"));
				myBook.setMbDate(rs.getDate("mb_date"));
				myBook.setMbStatus(rs.getString("mb_status"));
				
				list.add(myBook);
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return list;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////
	//결제
	//캐쉬증가
	public int cashPayMent(int pay ,String id) {
		int result = 0;
		
		try {
			conn();
			String sql = "update mem set mem_cash = mem_cash + ? , mem_totalcash = mem_totalcash + ? where mem_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, pay);
			pstmt.setInt(2, pay);
			pstmt.setString(3, id);
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	//카드머니 감소
	public int cardPayMent(int pay, String id) {
		int result2= 0;
		try {
			conn();
			String sql = "update card set money = money - ? where cvc = (select cvc from mem where mem_id = ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, pay);
			pstmt.setString(2,id);
			
			result2 = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result2;
	}
	
	//대여한 도서 삭제 
	public int delBook() {
		int result = 0;
		try {
			conn();
			String sql = "delete from mybook where mb_status = 'R' AND (sysdate-mb_date) >= 7";
			pstmt = conn.prepareStatement(sql);
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//추천장르
	public MyBook genreRecommend() {
		MyBook myBook = null;
		try {
			conn();
			String sql = "select b_genre , count(*)\r\n"
					+ "from mybook\r\n"
					+ "group by b_genre";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			if(rs.next()) {
				myBook = new MyBook();
				myBook.setbGenre(rs.getString("b_genre"));
			}else {
				myBook = null;
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return myBook;
	}
	
	
	
	
}
