package com.yedam.mybook;

import java.sql.Date;

public class MyBook {
//	B_ID           NUMBER(10)    
//	B_NAME         VARCHAR2(30)  
//	B_GENRE        VARCHAR2(30)  
//	B_SUMMARY      VARCHAR2(500) 
//	B_PUBLISHER    VARCHAR2(30)  
//	MB_DATE        DATE          
//	MB_STATUS      VARCHAR2(3)   

	private int bId;
	private String bName;
	private String bGenre;
	private String bSummary;
	private String bPublisher;
	private Date mbDate;
	private String mbStatus;
	
	
	public int getbId() {
		return bId;
	}
	public void setbId(int bId) {
		this.bId = bId;
	}
	public Date getMbDate() {
		return mbDate;
	}
	public void setMbDate(Date date) {
		this.mbDate = date;
	}
	public String getMbStatus() {
		return mbStatus;
	}
	public void setMbStatus(String mbStatus) {
		this.mbStatus = mbStatus;
	}
	public String getbName() {
		return bName;
	}
	public void setbName(String bName) {
		this.bName = bName;
	}
	public String getbGenre() {
		return bGenre;
	}
	public void setbGenre(String bGenre) {
		this.bGenre = bGenre;
	}
	public String getbSummary() {
		return bSummary;
	}
	public void setbSummary(String bSummary) {
		this.bSummary = bSummary;
	}
	public String getbPublisher() {
		return bPublisher;
	}
	public void setbPublisher(String bPublisher) {
		this.bPublisher = bPublisher;
	}
	
	
}
