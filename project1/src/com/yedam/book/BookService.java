package com.yedam.book;

import java.util.List;
import java.util.Scanner;

import com.yedam.mem.MemService;
import com.yedam.mybook.MyBook;
import com.yedam.mybook.MyBookDAO;
import com.yedam.mybook.MyBookService;

import BookReview.BookReviewDAO;

public class BookService {
	Scanner sc = new Scanner(System.in);
//	private int bId;
//	private String bName;
//	private int bRPrice;
//	private int bKPrice;
//	private String bGenre;
//	private int bGrade;
//	private String bSummary;
//	private String bPub;
	
	//전체도서조회
	public void getBookList() {
		List<Book> list = BookDAO.getInstance().getBookList();
		
		for(Book book : list) {
			System.out.println("==============================================");
			System.out.println("책 번호: "+book.getbId());
			System.out.println("책 이름: "+book.getbName());
			System.out.println("대여 가격: "+book.getbRPrice());
			System.out.println("소장 가격: "+book.getbKPrice());
			System.out.println("장르: "+book.getbGenre());
			System.out.println("평점: "+book.getbGrade()/BookReviewDAO.getInstance().count(book).getCount());
			System.out.println("줄거리: "+book.getbSummary());
			System.out.println("출판사: "+book.getbPub());
			System.out.println("==============================================");
		}
	}
	//장르별 조회
	public void getBookGenre() {
		String genre = "";
		
		System.out.println("조회할 장르를 입력하세요 : ");
		genre = sc.nextLine();
		
		List<Book> list = BookDAO.getInstance().getBookList();
		for(Book book : list) {
			if(book.getbGenre().equals(genre)) {
				System.out.println("==============================================");
				System.out.println("책 번호: "+book.getbId());
				System.out.println("책 이름: "+book.getbName());
				System.out.println("대여 가격: "+book.getbRPrice());
				System.out.println("소장 가격: "+book.getbKPrice());
				System.out.println("장르: "+book.getbGenre());
				System.out.println("평점: "+book.getbGrade()/BookReviewDAO.getInstance().count(book).getCount());
				System.out.println("줄거리: "+book.getbSummary());
				System.out.println("출판사: "+book.getbPub());
				System.out.println("==============================================");
			}/*else {
				System.out.println("올바른 장르가 아닙니다");
				break;
			}*/
		}
		
	}
	//제목 검색
	public void getBookName() {
		String name = "";
		
		System.out.println("조회할 제목을 입력하세요 : ");
		name = sc.nextLine();
		
		List<Book> list = BookDAO.getInstance().getBookList();
		for(Book book : list) {
			if(book.getbName().equals(name)) {
				System.out.println("==============================================");
				System.out.println("책 번호: "+book.getbId());
				System.out.println("책 이름: "+book.getbName());
				System.out.println("대여 가격: "+book.getbRPrice());
				System.out.println("소장 가격: "+book.getbKPrice());
				System.out.println("장르: "+book.getbGenre());
				System.out.println("평점: "+book.getbGrade()/BookReviewDAO.getInstance().count(book).getCount());
				System.out.println("줄거리: "+book.getbSummary());
				System.out.println("출판사: "+book.getbPub());
				System.out.println("==============================================");
			}/*else {
				System.out.println("올바른 제목이 아닙니다");
				break;
			}*/
		}
	}
	
	//도서구매
	public void bookBuy() {
		int b_id = 0;
		String b_rk = "";
		
		System.out.println("구매할 도서아이디를 입력하세요 :");
		b_id = Integer.parseInt(sc.nextLine());
		System.out.println("'대여'하시겠습니까 '소장'하시겠습니까 (R / K) :");
		b_rk = sc.nextLine();
		
		//랜탈
		if(b_rk.equals("R")) {
			Book book = new Book();
			MyBook mb = new MyBook();
			
			mb.setbId(b_id);
			mb = BookDAO.getInstance().duplicated(mb);
			
			if(mb == null) {
				book.setbId(b_id);
				
				int result = BookDAO.getInstance().bookRBuy(book);
				int result2 = BookDAO.getInstance().rPriceUpdate(MemService.memInfo.getMemId(), book);
				
				if(result == 1) {
					System.out.println("대여 완료 ~");
				}else if(result == 0) {
					System.out.println("구매가 정상적으로 이루어지지 않았습니다.");
				}
				
				if(result2 == 1) {
					System.out.println("캐시차감");
				}else if(result2 == 0) {
					System.out.println("캐시차감안됨");
				}
			}else if(mb != null) {
				System.out.println("이미 소장 혹은 대여중인 도서입니다.");
			}
			
		//소장
		}else if(b_rk.equals("K")) {
			Book book = new Book();
			MyBook mb = new MyBook();
			
			mb.setbId(b_id);
			mb = BookDAO.getInstance().duplicated(mb);
			
			if(mb == null) {
				book.setbId(b_id);
				
				int result = BookDAO.getInstance().bookKBuy(book);
				int result2 = BookDAO.getInstance().kPriceUpdate(MemService.memInfo.getMemId(), book);
				
				if(result == 1) {
					System.out.println("소장 완료 ~");
				}else if(result == 0) {
					System.out.println("구매가 정상적으로 이루어지지 않았습니다.");
				}
				
				if(result2 == 1) {
					System.out.println("캐시차감");
				}else if(result2 == 0) {
					System.out.println("캐시차감안됨");
				}
			}else if(mb != null) {
				System.out.println("이미 소장 혹은 대여중인 도서입니다.");
			}
			
			
		}else {
			System.out.println("잘못 입력하셨습니다.");
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
