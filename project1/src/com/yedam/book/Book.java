package com.yedam.book;

public class Book {
//	B_ID        NOT NULL NUMBER(10)    
//	B_NAME      NOT NULL VARCHAR2(30)  
//	B_R_PRICE   NOT NULL NUMBER(10)    
//	B_K_PRICE   NOT NULL NUMBER(10)    
//	B_GENRE              VARCHAR2(30)  
//	B_GRADE              NUMBER(5)     
//	B_SUMMARY            VARCHAR2(500) 
//	B_PUBLISHER          VARCHAR2(30)
	private int bId;
	private String bName;
	private int bRPrice;
	private int bKPrice;
	private String bGenre;
	private double bGrade;
	private String bSummary;
	private String bPub;
	public int getbId() {
		return bId;
	}
	public void setbId(int bId) {
		this.bId = bId;
	}
	public String getbName() {
		return bName;
	}
	public void setbName(String bName) {
		this.bName = bName;
	}
	public int getbRPrice() {
		return bRPrice;
	}
	public void setbRPrice(int bRPrice) {
		this.bRPrice = bRPrice;
	}
	public int getbKPrice() {
		return bKPrice;
	}
	public void setbKPrice(int bKPrice) {
		this.bKPrice = bKPrice;
	}
	public String getbGenre() {
		return bGenre;
	}
	public void setbGenre(String bGenre) {
		this.bGenre = bGenre;
	}
	public double getbGrade() {
		return bGrade;
	}
	public void setbGrade(double bGrade) {
		this.bGrade = bGrade;
	}
	public String getbSummary() {
		return bSummary;
	}
	public void setbSummary(String bSummary) {
		this.bSummary = bSummary;
	}
	public String getbPub() {
		return bPub;
	}
	public void setbPub(String bPub) {
		this.bPub = bPub;
	}
	
	
}
