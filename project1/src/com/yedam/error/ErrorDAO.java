package com.yedam.error;

import java.util.ArrayList;
import java.util.List;

import com.yedam.book.Book;
import com.yedam.common.DAO;
import com.yedam.mybook.MyBook;

import BookReview.BookReview;

public class ErrorDAO extends DAO {
	
	
	private static ErrorDAO errorDao = null;
	private ErrorDAO() {
		
	}
	public static ErrorDAO getInstance() {
		if(errorDao == null) {
			errorDao = new ErrorDAO();
		}
		return errorDao;
	}
	/////////////////////////////////////////////////////////////////////////////
	
	//오타,오역 insert
	public int error(Book book,String error) {
		int result = 0;
		try {
			conn();
			String sql = "insert into book_error (b_id , error) \r\n"
					+ "values (?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, book.getbId());
			pstmt.setString(2, error);
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally{
			disconn();
		}
		return result;
	}
	
	//mybook 소지 여부
	public MyBook duplicated(MyBook myBook) {
		try {
			conn();
			String sql = "select * from mybook where b_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,myBook.getbId());
			rs = pstmt.executeQuery();
			//mybook에 있는 책이면
			if(rs.next()) {
				myBook = new MyBook();
				myBook.setbId(rs.getInt("b_id"));
			//mybook에 없는 책이면
			}else {
				myBook = null;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return myBook;
	}
	
	//제보된 에러 보기
	public List<Error> getError(){
		List<Error> list = new ArrayList<>();
		try {
			conn();
			String sql = "select * from book_error";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Error er = new Error();
				er.setbId(rs.getInt("b_id"));
				er.setError(rs.getString("error"));
				list.add(er);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return list;
	}
	
	
	
	
	
	
	
	
	
	
	
}
