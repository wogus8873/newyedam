package com.yedam.error;

import java.util.List;
import java.util.Scanner;

import com.yedam.book.Book;
import com.yedam.mybook.MyBook;

import BookReview.BookReview;
import BookReview.BookReviewDAO;

public class ErrorService {
	Scanner sc = new Scanner(System.in);
	
	public void error() {
		MyBook mb = new MyBook();
		Book book = new Book();
		int id = 0;
		String error = "";
		
		System.out.println("제보할 도서아이디를 입력하세요 : ");
		id = Integer.parseInt(sc.nextLine());
		
		mb.setbId(id);
		mb = ErrorDAO.getInstance().duplicated(mb);
		if(mb != null) {
			System.out.println("제보할 내용을 작성해주세요 : ");
			error = sc.nextLine();
			book.setbId(id);
			
			int result = ErrorDAO.getInstance().error(book, error);
			
			if(result >= 1) {
				System.out.println("제보 완료");
			}else if(result == 0) {
				System.out.println("제보 실패");
			}
		}else {
			System.out.println("제보는 책을 소장하거나 대여하고있을 때 입력가능합니다.");
		}
	}
	//제보된 에러 조회
	public void getError() {
		List<Error> list = ErrorDAO.getInstance().getError();
		
		for(Error br : list) {
			System.out.println("도서번호: "+br.getbId());
			System.out.println("오타,오역: "+br.getError());
			System.out.println("===================================");
		}
	
	}
	
}
