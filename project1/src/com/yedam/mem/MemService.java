package com.yedam.mem;

import java.util.Scanner;

import com.yedam.mybook.MyBookDAO;

public class MemService {
	Scanner sc = new Scanner(System.in);
	public static Mem memInfo = null;
	
	
	//로그인
	public void login() {
		Mem mem = new Mem();
		String id = "";
		String pw = "";
		
		System.out.println("아이디 입력 : ");
		id = sc.nextLine();
		System.out.println("비밀번호 입력: ");
		pw = sc.nextLine();
		
		mem.setMemId(id);
		mem = MemDAO.getInstance().login(mem);
		
		if(mem != null) {
			if(mem.getMemPw().equals(pw)) {
				memInfo = mem;
				System.out.println("정상적으로 로그인 됐습니다.");
			}else {
				System.out.println("비밀번호가 틀립니다.");
			}
		}else {
			System.out.println("등록되지 않은 회원입니다.");
		}
	}
	//로그아웃
	public void logout() {
		memInfo = null;
		System.out.println("로그아웃 됐습니다.");
	}
	
	//회원조회 (단건) / 로그인 안 했을때 
	public void getMem() {
		String id;
		System.out.println("조회할 아이디 : ");
		id = sc.nextLine();
		
		Mem mem = MemDAO.getInstance().getMem(id);
		if(mem != null) {
			System.out.println("회원아이디 : "+ mem.getMemId());
			System.out.println("회원등급 : "+mem.getMemGrade());
		}else {
			System.out.println("등록된 회원이 없습니다.");
		}
	}
	
	//회원조회 (단건) /로그인 했을때 
	public void getMyMem() {
		System.out.println("아이디 : "+memInfo.getMemId());
		System.out.println("비밀번호 : "+memInfo.getMemPw());
		System.out.println("이름 : "+memInfo.getMemName());
		System.out.println("휴대폰 번호 : "+memInfo.getMemPhone());
		System.out.println("이메일 : "+memInfo.getMemEmail());
		System.out.println("카드 : "+memInfo.getCvc());
		System.out.println("회원등급 : "+memInfo.getMemGrade());
		System.out.println("남은 캐시 :"+memInfo.getMemCash());
		System.out.println("누적 캐시 :"+memInfo.getMemTotalCash());
	}
	
	//회원등록
	public void insertMem() {
		String id = "";
		String pw = "";
		String name = "";
		String phone = "";
		String email = "";
		int cvc = 0;
		Mem mem = new Mem();
		
		while(true) {
			System.out.println("등록할 아이디를 입력하세요 : ");
			id = sc.nextLine();
			mem.setMemId(id);
			
			mem = MemDAO.getInstance().login(mem);
			if(mem == null) {
				mem = new Mem();
				System.out.println("사용가능한 아이디입니다");
				break;
			}else {
				System.out.println("중복된 아이디 입니다.");
			}
		}
		System.out.println("비밀번호를 설정하세요 : ");
		pw = sc.nextLine();
		System.out.println("성함을 입력하세요 : ");
		name = sc.nextLine();
		System.out.println("-포함 휴대폰 번호를 입력하세요 : ");
		phone = sc.nextLine();
		System.out.println("이메일 주소를 입력하세요 : ");
		email = sc.nextLine();
		System.out.println("카드번호를 입력하세요 : ");
		cvc = Integer.parseInt(sc.nextLine());
		
		mem.setMemId(id);
		mem.setMemPw(pw);
		mem.setMemName(name);
		mem.setMemPhone(phone);
		mem.setMemEmail(email);
		mem.setCvc(cvc);
		
		int result = MemDAO.getInstance().insertMem(mem);
		
		if(result >= 1) {
			System.out.println("정상적으로 등록이 됐습니다.");
		}else if (result == 0){
			System.out.println("정상적으로 등록이 되지 않았습니다.");
		}
	}
	//비밀번호 조회(단건조회)
	public void getPw() {
		String id = "";
		int cvc = 0;
		
		System.out.println("조회할 아이디를 입력하세요 :");
		id = sc.nextLine();
		System.out.println("등록한 카드번호를 입력하세요 :");
		cvc = Integer.parseInt(sc.nextLine());
		
		Mem mem = MemDAO.getInstance().getMem(id);
		if(mem != null) {
			if(mem.getCvc() == cvc) {
				System.out.println(mem.getMemId()+"의 pw는 "+mem.getMemPw());
			}else {
				System.out.println("등록된 카드번호와 입력한 카드번호가 다릅니다");
			}
		}
		
	}
	
	//비밀번호 변경
	public void memUpdatePw() {
		System.out.println("변경할 비밀번호를 입력하세요 : ");
		memInfo.setMemPw(sc.nextLine());
		
		int result = MemDAO.getInstance().memUpdatePw(memInfo);
		
		if(result >= 1) {
			System.out.println("정상적으로 변경 됐습니다");
		}else if(result == 0) {
			System.out.println("비밀번호 변경 실패");
		}
	}
	//이메일 변경
	public void memUpdateEm() {
		System.out.println("변경할 이메일주소를 입력하세요 : ");
		memInfo.setMemEmail(sc.nextLine());
		
		int result = MemDAO.getInstance().memUpdateEm(memInfo);
		
		if(result >= 1) {
			System.out.println("정상적으로 변경 됐습니다");
		}else if(result == 0) {
			System.out.println("비밀번호 변경 실패");
		}
	}
	//탈퇴
	public void memDelete() {
		System.out.println("정말 탈퇴하시겠습니까? (Y / N) : ");
		String answer = sc.nextLine();
		
		if(answer.equals("Y")) {
			int result = MemDAO.getInstance().memDelete(memInfo);
			
			if(result >= 1) {
				System.out.println("정상적으로 탈퇴 됐습니다");
				logout();
			}else if(result == 0) {
				System.out.println("탈퇴 실패");
			}
		}else if(answer.equals("N")) {
			System.out.println("즐거운 독서 하세요");
		}
	}
	
	//승급전 
	public void memGradeUp() {
		String gold = "gold";
		String silver = "silver";
		
		if(memInfo.getMemTotalCash() >= 200000) {
			memInfo.setMemGrade(gold);
		}else if(memInfo.getMemTotalCash() >= 100000) {
			memInfo.setMemGrade(silver);
		}
		int result = MemDAO.getInstance().memGradeUp(memInfo);
		
		if(result == 1) {
			System.out.println("신분 상승 완료");
		}else if(result == 0) {
			System.out.println("신분 상승 실패");
		}
	}
	
	
	
	
	
}


