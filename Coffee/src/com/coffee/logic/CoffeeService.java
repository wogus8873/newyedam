package com.coffee.logic;

import java.util.List;
import java.util.Scanner;

public class CoffeeService {
	Scanner sc = new Scanner(System.in);
	
	
	//1.전체조회
	public void getCoffeeList() {
		List<Coffee> list = CoffeeDAO.getInstance().getCoffeeList();
		//list 안에는 Coffee 타입의 객체가 여러건 존재.
		//list 안에 있는 객체를 coffee에 넣어서 출력하는것
		//상향된 for문
		for(Coffee coffee : list) {
			System.out.println("메뉴 : "+coffee.getCoffeeMenu());
			System.out.println("가격 : "+coffee.getCoffeePrice());
			System.out.println("설명 : "+coffee.getCoffeeExplan());
			System.out.println("===================================");
		}
	}
	//2.단건조회
	public void getCoffee() {
		System.out.println("조회할 메뉴 입력 : ");
		String menu = sc.nextLine();
		
		//검색결과
		Coffee coffee = CoffeeDAO.getInstance().getCoffee(menu);
		
		if(coffee == null) {
			System.out.println("등록되지 않은 메뉴입니다.");
		}else {
			System.out.println("메뉴 : "+coffee.getCoffeeMenu());
			System.out.println("가격 : "+coffee.getCoffeePrice());
			System.out.println("설명 : "+coffee.getCoffeeExplan());
			System.out.println("===================================");
		}
		
			
	}
	
	//3.등록
	//java에서는 오토 commit이라 자동 등록
	public void insertCoffee() {
		Coffee coffee = new Coffee();
		String menu = "";
		int price = 0;
		String explain = "";
		
		System.out.println("메뉴등록 > ");
		System.out.println("메뉴 : ");
		menu = sc.nextLine();
		System.out.println("가격 : ");
		price = Integer.parseInt(sc.nextLine());
		System.out.println("설명 : ");
		explain = sc.nextLine();
		
		coffee.setCoffeeMenu(menu);
		coffee.setCoffeePrice(price);
		coffee.setCoffeeExplan(explain);
		coffee.setCoffeeSales(0);
		
		int result = CoffeeDAO.getInstance().insertCoffee(coffee);
		
		if(result ==1 ) {
			System.out.println("정상적으로 메뉴가 등록 됐습니다.");
		}else {
			System.out.println("메뉴가 등록되지 않았습니다.");
		}
		
	}
	//4.판매
	public void salesCoffee() {
		Coffee coffee = new Coffee();
		String menu = "";
		int salesCount = 0;
		
		System.out.println("판매 메뉴 : ");
		menu = sc.nextLine();
		System.out.println("판매 수량 : ");
		salesCount = Integer.parseInt(sc.nextLine());
		coffee.setCoffeeMenu(menu);
		coffee.setCoffeeSales(salesCount);
		
		int result = CoffeeDAO.getInstance().salesCoffee(coffee);
		if(result >= 1) {
			System.out.println(menu + salesCount + "건 판매완료");
		}else {
			System.out.println("판매실패");
		}
		
	}
	
	//5.삭제
	public void deleteMenu() {
		String menu = "";
		System.out.println("삭제메뉴 입력 : ");
		menu = sc.nextLine();
		
		int result = CoffeeDAO.getInstance().deleteMenu(menu);
		
		if(result >= 1) {
			System.out.println("정상적으로 삭제 됐습니다.");
		}else {
			System.out.println("메뉴가 삭제되지 않았습니다.");
		}
	}
	
	//6.매출
	//매출은 dao안 만들거임 기존에 만들어진거 그냥 가져오기만 하면 됨 가격과 판매수량 곱하면 매출이기때문
	//전체조회 호출해서 얘 안에 데이터 연산할거임
	public void storeSales() {
		System.out.println("또치 커피샵 매출 내역서");
		List<Coffee> list = CoffeeDAO.getInstance().getCoffeeList();
		int totalSales = 0;
		
		for(Coffee coffee : list) {
			System.out.println("메뉴 : "+coffee.getCoffeeMenu());
			System.out.println("가격 : "+coffee.getCoffeePrice());
			System.out.println("판매 수량 : "+coffee.getCoffeeSales());
			//판매금액 = 가격 * 판매수량
			System.out.println("판매 금액 : "+(coffee.getCoffeePrice()*coffee.getCoffeeSales()));
			totalSales += (coffee.getCoffeePrice()*coffee.getCoffeeSales());
			System.out.println("========================================");
			
		}
		System.out.println("총 판매 금액 : "+totalSales+"원");
	}
	
	
	
	
	
}
