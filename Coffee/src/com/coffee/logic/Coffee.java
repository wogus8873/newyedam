package com.coffee.logic;

public class Coffee {
	//Coffee에 대한 정보를 담고 출력할 때만 사용
	//DTO = DATA TRANSFER OBJECT (순수 정보)
	//VO = VALUE OBJECT (정보 + 살짝의 기능)
	//Beans = DTO VO 같은의미
	
	private String coffeeMenu;
	private int coffeePrice;
	private String coffeeExplan;
	private int coffeeSales;
	
	//private 라서 겟터 셋터 다 만들어줌 
	
	public String getCoffeeMenu() {
		return coffeeMenu;
	}
	public void setCoffeeMenu(String coffeeMenu) {
		this.coffeeMenu = coffeeMenu;
	}
	public int getCoffeePrice() {
		return coffeePrice;
	}
	public void setCoffeePrice(int coffeePrice) {
		this.coffeePrice = coffeePrice;
	}
	public String getCoffeeExplan() {
		return coffeeExplan;
	}
	public void setCoffeeExplan(String coffeeExplan) {
		this.coffeeExplan = coffeeExplan;
	}
	public int getCoffeeSales() {
		return coffeeSales;
	}
	public void setCoffeeSales(int coffeeSales) {
		this.coffeeSales = coffeeSales;
	}
	
	
	
	
	
	
	
	
	
	
}
