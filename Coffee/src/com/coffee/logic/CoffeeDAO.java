package com.coffee.logic;

import java.util.ArrayList;
import java.util.List;

import com.coffee.common.DAO;
//java -> jdbc -> db ->coffee table에 접근 -> 데이터조회 추가 수정 삭제 
public class CoffeeDAO extends DAO {
	//SingleTon(싱글톤)
	//하나만 만들어서 쓰면되는데 여러개 만드는것을 방지하는것 
	//단 하나의 객체만 존재하기ㅣ위해 만들어진것
	private static CoffeeDAO coffeeDao = null;
	private  CoffeeDAO() {
		
	}
	
	
	//CoffeeDao를 쓸 일이 있으면 객체 만드는것
	public static CoffeeDAO getInstance() {
		if(coffeeDao == null) {
			coffeeDao = new CoffeeDAO();
		}
		return coffeeDao;
	}

	//coffeeDAO : DB 에서 coffee Table 에 접근하기위해 쓰는 객체
	//전체 조회, 단건조회, 정보입력,수정,삭제 
	//CRUD : Create/Read / Update /Delete
	//1. 전체조회(List 활용 , 6매출
	public List<Coffee> getCoffeeList(){
		//전체조회 -> select 문의 결과를  다 가지고 와야함
		//List , set , map 
		//set, map 못 쓴 이유  - > 얘네는 순서가 없음
		//select order by 해서 가져와도 의미가없어서 
		//그래서 list 씀 -> 
		List<Coffee> list = new ArrayList<>();
		//list[0] => 첫번째 row의 데이터
		//list[1] => 두번째 row의 데이터 ....
		Coffee coffee = null;
		
		try {
			conn();
			String sql = "select * from coffee";
			//1. statement  - where 조건 있으면 쓰끼 불편 (where 조건 없는select)
			//2.preparedstatment - where 조건 있어도 쓰기 편함
			stmt = conn.createStatement();
			
			//Query 문 실행 및 결과 반환
			rs = stmt.executeQuery(sql);
			
			//next() 메소드 - 다음 row 에 데이터가 존재하면 true 아니면 false
			//getString , getInt()
			while(rs.next()) {
				coffee = new Coffee();
				//서로다른 로우를 서로다른 객체에 저장하기위해 구현한것
				coffee.setCoffeeMenu(rs.getString("coffee_menu"));
				coffee.setCoffeePrice(rs.getInt("coffee_price"));
				coffee.setCoffeeExplan(rs.getString("coffee_explain"));
				coffee.setCoffeeSales(rs.getInt("coffee_sales"));
				list.add(coffee);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return list;
	}
	
	//2.단건조회
	public Coffee getCoffee(String menu) {
		Coffee coffee = null;
		try { 
			conn();
			//preparedStatment경우
			String sql = "select * from coffee where coffee_menu = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, menu);
			//1번째로 들어가는 ? 에 menu를 집어넣는다 위에서 String  menu 받았죠? 
			rs = pstmt.executeQuery();
			//executeQuery는 select문에서만 가능
			//단건조회라 if 
			if(rs.next()) {
				coffee = new Coffee();
				
				coffee.setCoffeeMenu(rs.getString("coffee_menu"));
				coffee.setCoffeePrice(rs.getInt("coffee_price"));
				coffee.setCoffeeExplan(rs.getString("coffee_explain"));
				coffee.setCoffeeSales(rs.getInt("coffee_sales"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			disconn();
		}
		return coffee;
	}
	
	
	//3.메뉴등록             데이터등록할건데 커피클래스가 가진 필드를 사용해서 한번에 다 가져올거야 
	public int insertCoffee(Coffee coffee) {
		int result = 0;
		try {
			conn();
			String sql = "INSERT INTO coffee values (?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,coffee.getCoffeeMenu());
			pstmt.setInt(2,coffee.getCoffeePrice());
			pstmt.setString(3,coffee.getCoffeeExplan());
			pstmt.setInt(4, coffee.getCoffeeSales());
			
			//dml 사용할때쓰는 쿼리메소드
			result = pstmt.executeUpdate();
			//select -> executequery 
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			disconn();
		}
		
		
		return result;
	}
	
	//4.판매
	public int salesCoffee(Coffee coffee) {
		int result = 0;
		try {
			conn();
			String sql = "update coffee set coffee_sales = coffee_sales+? where coffee_menu = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, coffee.getCoffeeSales());;
			pstmt.setString(2, coffee.getCoffeeMenu());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
		
	}
	
	
	
	//5.메뉴삭제
	public int deleteMenu(String menu) {
		int result = 0;
		try {
			conn();
			String sql = "delete from coffee where coffee_menu = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, menu);
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		
		
		
		return result;
		
	}
	
	

	
	
	
	
}
