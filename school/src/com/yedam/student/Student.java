package com.yedam.student;

public class Student {
	private int stdId;
	private String stdName;
	private int stdClass;
	private int stdKor = -1;
	private int stdEng = -1;
	private int stdMath = -1;
	
	
	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}
	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}
	public int getStdClass() {
		return stdClass;
	}
	public void setStdClass(int stdClass) {
		this.stdClass = stdClass;
	}
	public int getStdKor() {
		return stdKor;
	}
	public void setStdKor(int stdKor) {
		this.stdKor = stdKor;
	}
	public int getStdEng() {
		return stdEng;
	}
	public void setStdEng(int stdEng) {
		this.stdEng = stdEng;
	}
	public int getStdMath() {
		return stdMath;
	}
	public void setStdMath(int stdMath) {
		this.stdMath = stdMath;
	}
	
	
	
	
	
	
	
	
}
