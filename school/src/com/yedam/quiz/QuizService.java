package com.yedam.quiz;

import java.util.List;
import java.util.Scanner;

import com.yedam.student.Student;
import com.yedam.student.StudentDAO;

public class QuizService {
	Scanner sc = new Scanner(System.in);
	
	//1.과목별 문제조회
	public void getQuizList() {
		String subject = "";
		int subjectId = 0;
		System.out.println("조회 과목 : ");
		subject = sc.nextLine();
		if(subject.equals("국어")) {
			subjectId = 1;
		}else if(subject.equals("영어")) {
			subjectId = 2;
		}else if(subject.equals("수학")) {
			subjectId = 3;
		}
		
		List<Quiz> list = QuizDAO.getInstance().getQuizList(subjectId);
		for(Quiz quiz : list) {
			System.out.println("==============="+subject+"===================");
			System.out.println("문제 : "+quiz.getExamQuest());
			System.out.println("정답 : "+quiz.getExamAnswer());
		}
	}
	//2.문제드ㅜㅇ록
	public void insertQuiz() {
		Quiz quiz = new Quiz();
		
		String subject = "";
		int subjectId = 0;
		System.out.println("조회 과목 : ");
		subject = sc.nextLine();
		
		if(subject.equals("국어")) {
			subjectId = 1;
		}else if(subject.equals("영어")) {
			subjectId = 2;
		}else if(subject.equals("수학")) {
			subjectId = 3;
		}
		quiz.setSubjectId(subjectId);
		
		System.out.println("문제 : ");
		quiz.setExamQuest(sc.nextLine());
		System.out.println("정답 : ");
		quiz.setExamAnswer(sc.nextLine());
		
		QuizDAO.getInstance().insertQuiz(quiz);
	}
	
	//시험
	public void exam() {
		//시험문제, 시험채점결과
		System.out.println("학번 입력하세요 : " );
		int stdId = Integer.parseInt(sc.nextLine());
		
		
		int subjectId = 0; 
		
		System.out.println("응시과목 : ");
		
		String subject = sc.nextLine();
		if(subject.equals("국어")) {
			subjectId = 1;
		}else if(subject.equals("영어")) {
			subjectId = 2;
		}else if(subject.equals("수학")) {
			subjectId = 3;
		}
		
		//채점할때쓸변수
		int score = 0;
		String answer = "";
		
		List<Quiz> list = QuizDAO.getInstance().getQuizList(subjectId);
		//순서대로 문제 출력
		for(Quiz quiz : list) {
			System.out.println(quiz.getExamQuest());
			System.out.println("정답 :");
			answer = sc.nextLine();
			if(quiz.getExamAnswer().equals(answer)) {
				//문제 맞출때마다 10점씩 누적
				score += 10;
			}
		}
		//시험이 다 끝난 시점
		//누구의 점수인지 알 수 없다.
		//누구에게 점수를 입력해줄지 생갹해야합니다..
		//학번입력하고 바로 시험치게 
		//학번 , 과목 , 채점결과 -> 입력이 아닌 학생정보를 업데이트 해줘야함. 
		//StudentDAO 에서 update 사용할 수 있게 바꿔줄거임
		
		//(취득점수 / (문제문항 * 10))*100 = 100점기준 취득점수 구하는공식
		//문제가 총 10문제 , 취득점수는 70
		//취득점수 = score
		score = (score*100 / (list.size()*10));
		
		//학번 , 100점기준 취득점수 , 과목 
		//stdId, score , subjectId
		Student std = new Student();
		std.setStdId(stdId);
		if(subjectId == 1) {
			std.setStdKor(score);
		}else if(subjectId == 2) {
			std.setStdEng(score);
		}else if(subjectId == 3) {
			std.setStdMath(score);
		}
		
		QuizDAO.getInstance().updateScore(std);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
