package com.yedam.exe;

import java.util.Scanner;

import com.yedam.quiz.QuizService;
import com.yedam.student.StudentService;

public class LoadingPage {
	Scanner sc = new Scanner(System.in);
	
	StudentService ss = new StudentService();
	QuizService qs = new QuizService();
	
	public LoadingPage() {
		run();
	}
	
	public void run() {
		while(true) {
			String selectMenu = "";
			menu();
			selectMenu = sc.nextLine();
			if(selectMenu.equals("1")) {
				select();
			}else if(selectMenu.equals("2")) {
				updateStudent();
			}else if(selectMenu.equals("3")) {
				quiz();
			}else if(selectMenu.equals("4")) {
				System.out.println("오와리다.ㅋㅋ");
				break;
			}
			
		}
	}
	//메뉴
	private void menu() {
		System.out.println("1.조회 | 2.등록,삭제 | 3.시험문제 | 4.종료");
		System.out.println("입력 : ");
	}
	//조회
	private void select() {
		String subMenu = "";
		System.out.println("1.전체 | 2.교실별 | 3.전체 성적 | 4.개인별 조회 | 5.시험응시유무 | 6.개인별성적 ");
		subMenu = sc.nextLine();
		
		switch(subMenu) {
		case "1":
			ss.getStudentList();  //전체 조회
			break;
		case "2":
			ss.getStudentClass(); //교실별 조회
			break;
		case "3":
			ss.getStudentPoint(); //전체성적 조회
			break;
		case "4":
			ss.getStudent();      //개인별 조회
			break;
		case "5":
			ss.getStudentExamConfirm(); //시험응시유무
			break;
		case "6":
			ss.getStudentPP();
			break;
		}
	}
	//등록,삭제
	private void updateStudent() {
		String subMenu = "";
		System.out.println("1.등록 | 2.삭제");
		subMenu = sc.nextLine();
		
		switch(subMenu) {
		case "1":
			ss.insertStudent();
			break;
		case "2":
			ss.deleteStudent();
			break;
		}
	}
	
	//시험문제
	private void quiz() {
		System.out.println("1.문제조회 | 2.문제등록 | 3.응시");
		String subMenu = sc.nextLine();
		
		switch(subMenu) {
		case "1":
			System.out.println("조회과목");
			qs.getQuizList();
			break;
		case "2":
			qs.insertQuiz();
			break;
		case "3":
			qs.exam();
			break;
		}
		
	}
	
	
	
	
	
	
	
	
}
