package com.yedam.oop;

public class Application {
	public static void main(String[] args) {
		//SmartPhone 클래스(설계도)를 토대로 iphone14Pro
		SmartPhone iphone14Pro = new SmartPhone("Apple","iphoen14Pro",500);
//		iphone14Pro.maker = "Apple";     //정보기입
//		iphone14Pro.name = "iphone14Pro";  
//		iphone14Pro.price = 100000;
//		iphone14Pro.price = 500;//덮어쓰기 가능
//		
		iphone14Pro.call();     //name iphone14Pro 라고 기입하고 설계도에서 끌어와서 기능 쓴것 
		iphone14Pro.hangUp();
		
		//필드정보 읽기
		System.out.println(iphone14Pro.maker);
		System.out.println(iphone14Pro.name);
		System.out.println(iphone14Pro.price);
		
		
		//smartPhone 클래스(설계도)
		SmartPhone zflip4 = new SmartPhone("samsung","zflip4",2000); //매개변수를 안 넣어서 오류 . 클래스 생성자에 아무것도 안 넣으면 해결 
											  //그렇게되면 밑단 내용처럼 값을 넣어줘야함
//		zflip4.maker = "samsung";
//		zflip4.name = "zflip4";
//		zflip4.price = 2000;
		
		zflip4.call();
		zflip4.hangUp();
		
		System.out.println(iphone14Pro.maker);//물건을 만들게되면 물건안에다가 데이터를 넣는거임 클래스에 데이터를 넣는게 아님.
											  //그래서 플립뒤에 아이폰 출력해도 애플나옴
		
		
		
		
		
		
		SmartPhone sony = new SmartPhone();
		
		//리턴타입 없는 메소드
		/*int a =*/ sony.getInfo(0); //가져온 데이터가 없는데 int a가 데이터 달라고해서 오류
		//리턴타입이 int인 메소드
		int b = sony.getInfo("int"); // b 는 0 이됨 return 0; 이라서
		System.out.println(b);
		//리턴타입이 String[]인 메소드
		String[] temp = sony.getInfo(args);
		System.out.println(temp.length);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
