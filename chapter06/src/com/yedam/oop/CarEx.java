package com.yedam.oop;

public class CarEx {
	public static void main(String[] args) {
		Car myCar = new Car("포르쉐");   //클래스 복붙	새로운것
		Car yourCar = new Car("벤츠");	//클래스 복붙    새로운것
		
		myCar.run(); //myCar에 복사된 메소드를 실행하는것
		yourCar.run();//yourCar에 복사된 메소드 실행하는것
		//그래서 결과물이 다름
		
		
		//정적 필드 , 메소드 부르는 방법
		//정적 멤버가 속해있는 클래스명.필드 또는 메소드명     <- 공식     Calculator 에 만들어둠
		//1)정적필드 가져오는 방법
		double piRatio = Calculator.pi;  // < Calculator 에 저장된 것 불러온거임
					//    얘    밑에있는  얘  불러와		
		System.out.println(piRatio);  
		
		
		//2)정적 메소드 가져오는 방법
		int result = Calculator.plus(5,6);
		System.out.println(result);
		
		//객체를 만들지 않았는데 써짐 ㅋㅋ 자바가 실행될때 메모리영역에 넣어서 가져와서 써서그럼.
		
		
		
		//특징 1) 모든 클래스에서 사용 가능 -> 공유의 기능
		//    2) 남용하면 메모리 누수(부족) 발생할 수 있음
		//	  3)☆주의할점
		//	  	- 정적 메소드에서 외부에 정의한 필드를 사용하려고 한다면,
		//		  static 이 붙은 필드 또는 메소드만 사용 가능
		//		  static 붙이지 않고 사용하고싶다면 해당 필드와 메소드가 속해있는 클래스를 인스턴스화 해서
		//        인스턴스 필드와 인스턴스 메소드를 dot(.) 연산자 사용해서 가져와서 사용
		//        Application20 참조 
		
		
		Person p1 = new Person("123123-123456","김또치");
		// final -> nation ssn     그래서 바꾸려고하면 오류
		System.out.println(p1.nation);
		System.out.println(p1.ssn);
		System.out.println(p1.name);
		
//		p1.nation = "USA"
		
		
		System.out.println(5*5*ConstantNo.PI);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
