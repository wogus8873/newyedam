package com.yedam.Access;

public class Singleton {
	//정적필드
	private static Singleton sigleton = new Singleton();   //변경할 수 없는 객체 만들어둠	
	//생성자
	private Singleton() {
														//객체 못 만들게 막아둠
	}
	//정적 메소드
	public static Singleton getInstance() {             
		return sigleton;								//변경할 수 없는 객체를 쓰게 함
	}
	
}
