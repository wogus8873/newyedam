package com.yedam.Access;

public class Access {
	//필드
	/*
	 * public 어디서든 누구나 다 접근 가능
	 * protected 상속 받은 상태에서 부모 자식간에 사용 가능(패키지가 달라도 사용가능)
	 * 		     패기키가 다른 사용 못 함, 같은 패키지에서만 사용가능
	 * default 패키지가 다른 사용 못함 , 같은 패키지 내에서만 사용 간응
	 * private 내가 속한 클래스에서만 사용가능
	 * */
	//접근제한자 ->이름 지어서 사용(변수,클래스,메소드 등등)하는 친구들  다 사용가능 
	public String free;
	protected String parent;
	private String privacy;
	public String basic;
	
	
	
	//생성자    
	public Access() {
		
	}
//	private Access() {  //접근제한자 붙임
//		
//	}
	
	//메소드
//	public void run() {  //접근제한자 붙임 
//		
//	}
	public void free() {
		System.out.println("접근가능");
		privacy();
	}
	private void privacy() {          // private 라 사용 못 함  r근데 free메소드안에 써서 (내부에서 쓸 수 있음) 사용
		System.out.println("접근불가"); // 번거롭게 쓰는 이유는 보안 회사에서만든 프라이배이스 를 아무기능없는 프리에 넣어서 외부에서 안보이게 사용
	}
	
	
	
}
