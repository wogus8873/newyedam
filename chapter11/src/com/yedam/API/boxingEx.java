package com.yedam.API;

public class boxingEx {
	public static void main(String[] args) {
		//Boxing 기본타입을 객체로 포장
		//Boxing
		Integer obj1 = new Integer(100);
		Integer obj2 = new Integer("200");
		Integer obj3 = new Integer("300");
		//다른방법
		Integer obj4 = Integer.valueOf(100);
		Integer obj5 = Integer.valueOf("400");
		
		
		
		//unBoxing
		int value1 = obj1.intValue();
		int value2 = obj2.intValue();//intValue 로 문자열도 숫자로 데이터를 푼다
		int value3 = obj3.intValue();//intValue 로 문자열도 숫자로 데이터를 푼다
		
		int value4 = obj4.intValue();
		int value5 = obj5.intValue();
		
		System.out.println(value1);
		System.out.println(value2);
		System.out.println(value3);
		
		System.out.println(value4);
		System.out.println(value5);
		
		
		//자동 Boxing
		
		Integer obj6 = 500;   //객체
		System.out.println(obj6);
		
		
		//대입시 자동 unBoxing
		int value6 = obj6;  //객체를 기본타입에 넣음 원래 안 되는건데 자동으로 변환?되는듯
		System.out.println(value6);
		
		//연산시 자동 unBoxing
		int value7 = obj6 + 200;
		System.out.println(value7);
		
		
		
		//포장값 비교
		Integer obj7 = 300;
		Integer obj8 = 300;
		
		System.out.println(obj7 == obj8);  //같은 데이터를 넣었다고해도 주소값을 비교하는거라 false
		System.out.println(obj7.intValue() == obj8.intValue()); // 데이터를 꺼내와서 비교하는거라 true
		
		Boolean obj9 =true;
		Boolean obj10 = true;
		System.out.println("Boolean 비교 : "+(obj9 == obj10));
		
		Byte obj11 = -100;
		Byte obj12 = -100;
		System.out.println("Byte비교 : "+(obj11 == obj12));  //타입별 범위가있는데 그 범위안에있으면 ==도 데이터비교가 됨
		
		
		
		
		
		
	}
}
