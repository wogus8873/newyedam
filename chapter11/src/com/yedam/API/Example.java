package com.yedam.API;

import java.util.HashMap;

public class Example {
	public static void main(String[] args) {
		Object obj1 = new Object();
		Object obj2 = new Object();
		
		System.out.println(obj1);		
		System.out.println(obj2);
		
		boolean result = obj1.equals(obj2);
		System.out.println(result);
		
		result = (obj1 == obj2);
		System.out.println(result);
		
		
		//재정의한 equals를 통해 객체비교
		
		Member obj3 = new Member("blue");
		Member obj4 = new Member("blue");
		Member obj5 = new Member("red");
		
		
		if(obj3.equals(obj4)) {
			System.out.println("3과4는동일");
			
		}else {
			System.out.println("3과4는다름");
		}
		
		if(obj3.equals(obj5)) {
			System.out.println("3과5는 동일");
		}else {
			System.out.println("3과5는 다름");
		}
		
		//사물함에 꺼낼때 쓸 열쇠랑 자료?;
		HashMap<Key,String> hashMap = new HashMap<>();
		//new Key(1) -> 100번지 -> 1 (hashcode는 객체의 고유한 숫자(주소값))
		hashMap.put(new Key(1),"홍길동"); // 1로 홍길동을 hashmap에 저장 -> 홍길동
		//new Key(1) -> 100번지 아님-> 3 
		String value = hashMap.get(new Key(1));//1 -> hashmpa -> 홍길동
		
		System.out.println(value);
		
		
		
		
		
	}
}
